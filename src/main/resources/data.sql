CREATE TABLE roles (
   id INT AUTO_INCREMENT PRIMARY KEY,
   name VARCHAR(50) NOT NULL
);
INSERT INTO roles(name) VALUES('ROLE_DEVELOPER');
INSERT INTO roles(name) VALUES('ROLE_MANAGER');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');

CREATE TABLE users (
        id INT AUTO_INCREMENT PRIMARY KEY,
        email VARCHAR(120),
        password VARCHAR(120),
        username VARCHAR(120)
    );

    create table user_roles (
            user_id INT not null,
            role_id INT not null,
            primary key (user_id, role_id)
        );

    insert
        into
            users
            (id, email, password, username)
        values
            (1, 'anravindran@altimetrik.com', '$2a$10$/l8tTj77wNDGoLzPjWaxvurWfdx.eN3nmKKiyF2St2MDu.JNQsr8e', 'anravindran@altimetrik.com');

    insert
            into
                user_roles
                (user_id, role_id)
            values
                 (1, 2);

--pass : autoghe@123

