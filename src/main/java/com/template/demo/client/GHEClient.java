package com.template.demo.client;

import org.kohsuke.github.GitHub;
import org.kohsuke.github.GitHubBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@ConfigurationProperties(prefix = "github")
public class GHEClient {

    private String TOKEN;

    public GitHub githubClient() throws IOException {
        GitHub oauthGitHubClient = new GitHubBuilder().withOAuthToken("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").build();
        //GitHub basicGitHub = new GitHubBuilder().withPassword("altimetrik-airbnb", "rssaanish@5").build();
        return oauthGitHubClient;
    }
}
