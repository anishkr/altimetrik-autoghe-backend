package com.template.demo.client;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.template.demo.config.JiraConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URI;

@Component
public class JiraClient {
    private JiraConfig jiraConfig;
    @Autowired
    public JiraClient(JiraConfig jiraConfig) {
        this.jiraConfig = jiraConfig;
    }
    public JiraRestClient getJiraRestClient() {
        return new AsynchronousJiraRestClientFactory()
                .createWithBasicHttpAuthentication(URI.create(jiraConfig.getURL()),
                        jiraConfig.getUSERNAME(),
                        jiraConfig.getPASSWORD());
    }
}
