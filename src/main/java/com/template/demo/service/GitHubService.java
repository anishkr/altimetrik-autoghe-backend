package com.template.demo.service;

import com.template.demo.dto.JiraPermissionRequest;
import org.kohsuke.github.GHOrganization;

import java.io.IOException;
import java.util.List;

public interface GitHubService {

    void addUserToOrganization(String gheUserName, String organization) throws IOException;

    void addUserToOrganizationRepository(String gheUserName, String repository) throws IOException;

    GHOrganization getOrganizationByName(String organization) throws IOException;

    boolean hasMember(String gheUserName, String organization) throws IOException;

    void addUserToGHE(List<JiraPermissionRequest> jiraPermissionRequestList) throws IOException;
}
