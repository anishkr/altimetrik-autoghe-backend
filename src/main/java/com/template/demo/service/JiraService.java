package com.template.demo.service;

import com.template.demo.dto.Issue;
import com.template.demo.dto.RejectIssue;

import java.util.List;

public interface JiraService {
    List<Issue> getJiraOpenedIssues(String currentUser);

    List<Issue> getJiraRejectedIssues(String currentUser);

    List<Issue> getJiraDoneIssues(String currentUser);

    void postCommentByKey(String jiraIssueKey, String comment);

    Issue getIssueByKey(String jirKey);

    void updateIssueStatus(String jiraKey, String status);

    void rejectIssue(RejectIssue rejectIssue);
}
