package com.template.demo.service.impl;

import com.template.demo.dataloader.GitHubDataloader;
import com.template.demo.dto.JiraPermissionRequest;
import com.template.demo.security.services.UserDetailsImpl;
import com.template.demo.service.GitHubService;
import com.template.demo.service.JiraService;
import org.kohsuke.github.GHOrganization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class GitHubServiceImpl implements GitHubService {

    private GitHubDataloader gitHubDataloader;
    private JiraService jiraService;

    @Autowired
    public GitHubServiceImpl(GitHubDataloader gitHubDataloader, JiraService jiraService) {
        this.gitHubDataloader = gitHubDataloader;
        this.jiraService = jiraService;
    }

    @Override
    public void addUserToOrganization(String gheUserName, String organization) throws IOException {
        gitHubDataloader.addUserToOrganization(gheUserName, organization);
    }

    @Override
    public void addUserToOrganizationRepository(String gheUserName, String repository) throws IOException {
        gitHubDataloader.addUserToOrganizationRepository(gheUserName, repository);
    }

    @Override
    public GHOrganization getOrganizationByName(String organization) throws IOException {
        return gitHubDataloader.getOrganizationByName(organization);
    }

    @Override
    public boolean hasMember(String gheUserName, String organization) throws IOException {
        return gitHubDataloader.hasMember(gheUserName, organization);
    }

    @Override
    public void addUserToGHE(List<JiraPermissionRequest> jiraPermissionRequestList)  {
        jiraPermissionRequestList.forEach(jiraPermissionRequest -> {
            String requesterGHEUserName = jiraPermissionRequest.getGheUserName();
            String requesterOrganization = jiraPermissionRequest.getOrganizationName();
            UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                    .getPrincipal();
            String username = userDetails.getUsername();
            try {
                if(!hasMember(requesterGHEUserName, requesterOrganization)) {
                    addUserToOrganization(requesterGHEUserName, requesterOrganization);
                }
                addUserToOrganizationRepository(requesterGHEUserName,
                        jiraPermissionRequest.getRepositoryName());
                jiraService.postCommentByKey(jiraPermissionRequest.getJiraKey(), "Ticket was approved by " + username + " through AutoGHE App");
                jiraService.updateIssueStatus(jiraPermissionRequest.getJiraKey(), "Done");
            }catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            }
        });
    }
}
