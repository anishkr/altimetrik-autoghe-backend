package com.template.demo.service.impl;

import com.atlassian.jira.rest.client.api.domain.Issue;
import com.template.demo.dataloader.JiraDataloader;
import com.template.demo.dto.Comment;
import com.template.demo.dto.JiraPermissionRequest;
import com.template.demo.dto.RejectIssue;
import com.template.demo.service.JiraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class JiraServiceImpl implements JiraService {
    private JiraDataloader jiraDataloader;

    @Autowired
    public JiraServiceImpl(JiraDataloader jiraDataloader) {
        this.jiraDataloader = jiraDataloader;
    }

    @Override
    public List<com.template.demo.dto.Issue> getJiraOpenedIssues(String currentUser) {
        Iterable<Issue> jiraIssues = jiraDataloader.getJiraOpenedIssues(currentUser);
        return convertIssues(jiraIssues);
    }

    @Override
    public List<com.template.demo.dto.Issue> getJiraRejectedIssues(String currentUser) {
        Iterable<Issue> jiraIssues = jiraDataloader.getJiraRejectedIssues(currentUser);
        return convertIssues(jiraIssues);
    }

    @Override
    public List<com.template.demo.dto.Issue> getJiraDoneIssues(String currentUser) {
        Iterable<Issue> jiraIssues = jiraDataloader.getJiraDoneIssues(currentUser);
        return convertIssues(jiraIssues);
    }

    @Override
    public void postCommentByKey(String jiraIssueKey, String comment) {
        jiraDataloader.postCommentByKey(jiraIssueKey, comment);
    }

    @Override
    public com.template.demo.dto.Issue getIssueByKey(String jirKey) {
        return convertIssue(jiraDataloader.getIssueByKey(jirKey));
    }

    @Override
    public void updateIssueStatus(String jiraKey, String status) {
        jiraDataloader.updateIssueStatus(jiraKey, status);
    }

    @Override
    public void rejectIssue(RejectIssue rejectIssue) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        String username = userDetails.getUsername();
        jiraDataloader.postCommentByKey(rejectIssue.getJiraKey(), "Ticket was reject by " + username + " and reason is "+ rejectIssue.getComment());
        jiraDataloader.updateIssueStatus(rejectIssue.getJiraKey(), "Reject");
    }

    private List<com.template.demo.dto.Issue> convertIssues(Iterable<Issue> jiraissues) {
        List<com.template.demo.dto.Issue> issues = new ArrayList<>();
        for(Issue jiraIssue : jiraissues)    {
            issues.add(convertIssue(jiraIssue));
        }
        return issues;
    }

    private com.template.demo.dto.Issue convertIssue(Issue jiraIssue) {
        com.template.demo.dto.Issue issue = new com.template.demo.dto.Issue();
        issue.setId(jiraIssue.getId());
        issue.setJiraKey(jiraIssue.getKey());
        issue.setStatus(jiraIssue.getStatus().getName());
        issue.setIssueName(jiraIssue.getSummary().split(":")[0]);
        issue.setIssueDescription(jiraIssue.getDescription());
        issue.setIssueCreatedDate(jiraIssue.getCreationDate());
        issue.setLastUpdatedDate(jiraIssue.getUpdateDate());
        issue.setReporter(jiraIssue.getReporter().getDisplayName());
        issue.setReporterEmail(jiraIssue.getReporter().getEmailAddress());
        issue.setComments(convertComments(jiraIssue.getComments()));
        issue.setJiraPermissionRequests(getPermissionRequest(jiraIssue.getSummary(), jiraIssue.getDescription(), jiraIssue.getKey()));
        return issue;
    }

    private List<Comment> convertComments(Iterable<com.atlassian.jira.rest.client.api.domain.Comment> jiraComments) {
        List<Comment> comments = new ArrayList<>();
        for (com.atlassian.jira.rest.client.api.domain.Comment c : jiraComments) {
            comments.add(convertComment(c));
        }
        return comments;
    }

    private Comment convertComment(com.atlassian.jira.rest.client.api.domain.Comment jiraComment) {
        Comment comment = new Comment();
        comment.setCommentId(jiraComment.getId());
        comment.setComment(jiraComment.getBody());
        comment.setCreatedCommentDate(jiraComment.getCreationDate());
        comment.setLastUpdatedCommentDate(jiraComment.getUpdateDate());
        return comment;
    }

    private JiraPermissionRequest getPermissionRequest(String summary, String description, String jiraKey) {
        JiraPermissionRequest jiraPermissionRequest = new JiraPermissionRequest();
        jiraPermissionRequest.setRepositoryName(summary.split(":")[1]);
        jiraPermissionRequest.setOrganizationName(summary.split(":")[1].split("/")[0]);
        if("mailto".contains(description)) {
            jiraPermissionRequest.setGheUserName(description.substring( 1, description.length() - 1 ).split(":")[1]);
        }else {
            jiraPermissionRequest.setGheUserName(description);
        }
        jiraPermissionRequest.setJiraKey(jiraKey);
        return jiraPermissionRequest;
    }
}
