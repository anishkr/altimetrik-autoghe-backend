package com.template.demo.service.impl;

import com.template.demo.dto.Issue;
import com.template.demo.dto.JiraPermissionRequest;
import com.template.demo.service.EmailService;
import com.template.demo.service.GitHubService;
import com.template.demo.service.JiraService;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@ConditionalOnProperty(name = "spring.enable.scheduling")
public class ScheduleService {

    private final JiraService jiraService;
    private final GitHubService gitHubService;
    private final EmailService emailService;

    @Autowired
    public ScheduleService(JiraService jiraService,
                           GitHubService gitHubService,
                           EmailService emailService) {
        this.jiraService = jiraService;
        this.gitHubService = gitHubService;
        this.emailService = emailService;
    }

    @Scheduled(cron = "0 11 11 11 11 ?")
    public void cronJobSch() throws IOException {
        List<Issue> jiraIssues = jiraService.getJiraOpenedIssues("");
        List<Issue> updatedIssues = new ArrayList<>();
        for(Issue issue : jiraIssues) {
            JiraPermissionRequest jiraPermissionRequest = new JiraPermissionRequest();
            jiraPermissionRequest.setGheUserName("anishkr");
            jiraPermissionRequest.setOrganizationName("altimetrik-airbnb-test");
            jiraPermissionRequest.setRepositoryName("altimetrik-airbnb-test/auto-ghe-test");
            issue.setJiraPermissionRequests(jiraPermissionRequest);
            String requesterGHEUserName = issue.getJiraPermissionRequests().getGheUserName();
            String requesterOrganization = issue.getJiraPermissionRequests().getOrganizationName();
            if(!gitHubService.hasMember(requesterGHEUserName, requesterOrganization)) {
                gitHubService.addUserToOrganization(requesterGHEUserName, requesterOrganization);
            }
            gitHubService.addUserToOrganizationRepository(requesterGHEUserName,
                    issue.getJiraPermissionRequests().getRepositoryName());
            jiraService.updateIssueStatus(issue.getJiraKey(), "Done");
            updatedIssues.add(jiraService.getIssueByKey(issue.getJiraKey()));
            System.out.println("job executed");
        }
    }


    @Scheduled(cron = "0 11 11 11 11 ?")
    public void cronJobForMailing() throws IOException, TemplateException, MessagingException {

    }

}
