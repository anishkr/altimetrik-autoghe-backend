package com.template.demo.controller;

import com.template.demo.dto.Comment;
import com.template.demo.dto.GHERequest;
import com.template.demo.dto.JiraPermissionRequest;
import com.template.demo.service.GitHubService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/ghe")
public class GitHubRestController {

    private GitHubService gitHubService;

    @Autowired
    public GitHubRestController(GitHubService gitHubService) {
        this.gitHubService = gitHubService;
    }

    @PostMapping("/addUsers")
    @PreAuthorize("hasRole('MANAGER') or hasRole('ADMIN')")
    public ResponseEntity<?> addUserToGHE(@RequestBody GHERequest gheRequest) {
        try {
            gitHubService.addUserToGHE(gheRequest.getJiraPermissionRequest());
            return new ResponseEntity<>("Succesfully approved", HttpStatus.ACCEPTED);
        }catch (Exception e) {
            return new ResponseEntity<>(String.format("Internal server error %s", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
