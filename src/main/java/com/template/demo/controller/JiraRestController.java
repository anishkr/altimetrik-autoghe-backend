package com.template.demo.controller;

import com.template.demo.dto.Comment;
import com.template.demo.dto.Issue;
import com.template.demo.dto.RejectIssue;
import com.template.demo.service.EmailService;
import com.template.demo.service.JiraService;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = {"*", "http://localhost:3000"})
@RestController
@RequestMapping("/api/v1/jira")
public class JiraRestController {
    private JiraService jiraService;

    @Autowired
    public JiraRestController(JiraService jiraService, EmailService emailService) {
        this.jiraService = jiraService;
        this.emailService = emailService;
    }

    public EmailService emailService;

    @GetMapping("/getIssues")
    @PreAuthorize("hasRole('MANAGER') or hasRole('ADMIN')")
    public ResponseEntity<?> getAllJiraOpenedIssues(HttpServletRequest request) throws TemplateException, MessagingException, IOException {
        return new ResponseEntity<>(jiraService.getJiraOpenedIssues(request.getUserPrincipal().getName()), HttpStatus.OK);
    }

    @GetMapping("/getRejectIssues")
    @PreAuthorize("hasRole('MANAGER') or hasRole('ADMIN')")
    public ResponseEntity<?> getAllJiraRejectedIssues(HttpServletRequest request) throws TemplateException, MessagingException, IOException {
        return new ResponseEntity<>(jiraService.getJiraRejectedIssues(request.getUserPrincipal().getName()), HttpStatus.OK);
    }

    @GetMapping("/getCompletedIssues")
    @PreAuthorize("hasRole('MANAGER') or hasRole('ADMIN')")
    public ResponseEntity<?> getAllJiraDoneIssues(HttpServletRequest request) throws TemplateException, MessagingException, IOException {
        return new ResponseEntity<>(jiraService.getJiraDoneIssues(request.getUserPrincipal().getName()), HttpStatus.OK);
    }

    @PostMapping("/postComment")
    @PreAuthorize("hasRole('MANAGER') or hasRole('ADMIN')")
    public ResponseEntity<?> postComment(@RequestBody Comment comment) {
        jiraService.postCommentByKey(comment.getJiraKey(), comment.getComment());
        return new ResponseEntity<>("Succesfully approved", HttpStatus.ACCEPTED);
    }

    @GetMapping("/getIssue/{jiraKey}")
    @PreAuthorize("hasRole('MANAGER') or hasRole('ADMIN')")
    public ResponseEntity<?> getIssue(@PathVariable String jiraKey) {
        return new ResponseEntity<>(jiraService.getIssueByKey(jiraKey), HttpStatus.OK);
    }

    @PostMapping("/rejectIssue")
    @PreAuthorize("hasRole('MANAGER') or hasRole('ADMIN')")
    public ResponseEntity<?> rejectIssue(@RequestBody RejectIssue rejectIssue) {
        jiraService.rejectIssue(rejectIssue);
        return new ResponseEntity<>("Issue rejected", HttpStatus.OK);
    }
}
