package com.template.demo.dto;

public class JiraPermissionRequest {
    private String gheUserName;
    private String organizationName;
    private String repositoryName;
    private String jiraKey;

    public String getGheUserName() {
        return gheUserName;
    }

    public void setGheUserName(String gheUserName) {
        this.gheUserName = gheUserName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public String getJiraKey() {
        return jiraKey;
    }

    public void setJiraKey(String jiraKey) {
        this.jiraKey = jiraKey;
    }
}
