package com.template.demo.dto;

import java.util.List;

public class GHERequest {
    private List<JiraPermissionRequest> jiraPermissionRequest;

    public List<JiraPermissionRequest> getJiraPermissionRequest() {
        return jiraPermissionRequest;
    }

    public void setJiraPermissionRequest(List<JiraPermissionRequest> jiraPermissionRequest) {
        this.jiraPermissionRequest = jiraPermissionRequest;
    }
}
