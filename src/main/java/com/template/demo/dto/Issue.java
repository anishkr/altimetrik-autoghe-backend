package com.template.demo.dto;

import org.joda.time.DateTime;

import java.util.List;

public class Issue {

    private Long id;
    private String jiraKey;
    private String status;
    private String issueName;
    private String issueDescription;
    private String reporter;
    private String reporterEmail;
    private DateTime issueCreatedDate;
    private DateTime lastUpdatedDate;
    private List<Comment> comments;
    private JiraPermissionRequest jiraPermissionRequests;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJiraKey() {
        return jiraKey;
    }

    public void setJiraKey(String jiraKey) {
        this.jiraKey = jiraKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIssueName() {
        return issueName;
    }

    public void setIssueName(String issueName) {
        this.issueName = issueName;
    }

    public String getIssueDescription() {
        return issueDescription;
    }

    public void setIssueDescription(String issueDescription) {
        this.issueDescription = issueDescription;
    }

    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    public String getReporterEmail() {
        return reporterEmail;
    }

    public void setReporterEmail(String reporterEmail) {
        this.reporterEmail = reporterEmail;
    }

    public DateTime getIssueCreatedDate() {
        return issueCreatedDate;
    }

    public void setIssueCreatedDate(DateTime issueCreatedDate) {
        this.issueCreatedDate = issueCreatedDate;
    }

    public DateTime getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(DateTime lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public JiraPermissionRequest getJiraPermissionRequests() {
        return jiraPermissionRequests;
    }

    public void setJiraPermissionRequests(JiraPermissionRequest jiraPermissionRequests) {
        this.jiraPermissionRequests = jiraPermissionRequests;
    }
}
