package com.template.demo.dto;

import org.joda.time.DateTime;

public class Comment {
    private Long commentId;
    private String jiraKey;
    private String comment;
    private DateTime createdCommentDate;
    private DateTime lastUpdatedCommentDate;

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getJiraKey() {
        return jiraKey;
    }

    public void setJiraKey(String jiraKey) {
        this.jiraKey = jiraKey;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public DateTime getCreatedCommentDate() {
        return createdCommentDate;
    }

    public void setCreatedCommentDate(DateTime createdCommentDate) {
        this.createdCommentDate = createdCommentDate;
    }

    public DateTime getLastUpdatedCommentDate() {
        return lastUpdatedCommentDate;
    }

    public void setLastUpdatedCommentDate(DateTime lastUpdatedCommentDate) {
        this.lastUpdatedCommentDate = lastUpdatedCommentDate;
    }
}
