package com.template.demo.models;

public enum ERole {
    ROLE_DEVELOPER,
    ROLE_MANAGER,
    ROLE_ADMIN
}
