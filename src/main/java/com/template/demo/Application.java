package com.template.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author auto-ghe
 */
@EnableAsync
@EnableScheduling
@SpringBootApplication(scanBasePackages = { "com.template" })
@EntityScan("com.template.demo.*")
public class Application {
	public static void main(final String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
