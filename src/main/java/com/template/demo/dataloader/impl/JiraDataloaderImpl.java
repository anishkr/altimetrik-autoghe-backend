package com.template.demo.dataloader.impl;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.Comment;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.Transition;
import com.atlassian.jira.rest.client.api.domain.input.TransitionInput;
import com.template.demo.client.JiraClient;
import com.template.demo.dataloader.JiraDataloader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JiraDataloaderImpl implements JiraDataloader {

    private JiraClient jiraClient;
    private String currentUserName = "anravindran\\u0040altimetrik.com";

    @Autowired
    public JiraDataloaderImpl(JiraClient jiraClient) {
        this.jiraClient = jiraClient;
    }

    @Override
    public Iterable<Issue> getJiraOpenedIssues(String currentUser) {
        JiraRestClient jiraRestClient = jiraClient.getJiraRestClient();
        String user = currentUser.replace("@", "\\u0040");
        return jiraRestClient.getSearchClient()
                .searchJql("project = auto-ghe AND status in (Open) AND assignee IN (" + user + ")")
                .claim()
                .getIssues();
    }

    @Override
    public Iterable<Issue> getJiraRejectedIssues(String currentUser) {
        JiraRestClient jiraRestClient = jiraClient.getJiraRestClient();
        String user = currentUser.replace("@", "\\u0040");
        return jiraRestClient.getSearchClient()
                .searchJql("project = auto-ghe AND status in (Reject) AND assignee IN (" + user + ")")
                .claim()
                .getIssues();
    }

    @Override
    public Iterable<Issue> getJiraDoneIssues(String currentUser) {
        JiraRestClient jiraRestClient = jiraClient.getJiraRestClient();
        String user = currentUser.replace("@", "\\u0040");
        return jiraRestClient.getSearchClient()
                .searchJql("project = auto-ghe AND status in (Done) AND assignee IN (" + user + ")")
                .claim()
                .getIssues();
    }

    @Override
    public void postCommentByKey(String jiraIssueKey, String comment) {
        JiraRestClient jiraRestClient = jiraClient.getJiraRestClient();
        Issue issue = jiraRestClient.getIssueClient().getIssue(jiraIssueKey).claim();
        jiraRestClient.getIssueClient().addComment(issue.getCommentsUri(), Comment.valueOf(comment));
    }

    @Override
    public Issue getIssueByKey(String jiraKey) {
        JiraRestClient jiraRestClient = jiraClient.getJiraRestClient();
        return jiraRestClient.getIssueClient().getIssue(jiraKey).claim();
    }

    @Override
    public void updateIssueStatus(String jiraKey, String status) {
        Issue issue = getIssueByKey(jiraKey);
        JiraRestClient jiraRestClient = jiraClient.getJiraRestClient();
        IssueRestClient issueClient = jiraRestClient.getIssueClient();

        Iterable<Transition> transitions = issueClient.getTransitions(issue).claim();

        for(Transition t : transitions){
            if(t.getName().equals(status)) {
                TransitionInput input = new TransitionInput(t.getId());
                issueClient.transition(issue, input).claim();
            }
        }
    }
}
