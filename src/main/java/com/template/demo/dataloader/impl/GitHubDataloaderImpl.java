package com.template.demo.dataloader.impl;

import com.template.demo.client.GHEClient;
import com.template.demo.dataloader.GitHubDataloader;
import org.kohsuke.github.GHOrganization;
import org.kohsuke.github.GHUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class GitHubDataloaderImpl implements GitHubDataloader {

    public GHEClient gheClient;

    @Autowired
    public GitHubDataloaderImpl(GHEClient gheClient) {
        this.gheClient = gheClient;
    }

    @Override
    public void addUserToOrganization(String gheUserName, String organization) throws IOException {
        getOrganizationByName(organization).add(getGitHubUserByUserName(gheUserName), GHOrganization.Role.MEMBER);
    }

    @Override
    public void addUserToOrganizationRepository(String gheUserName, String repository) throws IOException {
        gheClient.githubClient().getRepository(repository).addCollaborators(getGitHubUserByUserName(gheUserName));
    }

    @Override
    public GHUser getGitHubUserByUserName(String gheUserName) throws IOException {
        return gheClient.githubClient().getUser(gheUserName);
    }

    @Override
    public boolean hasMember(String gheUserName, String organization) throws IOException {
        return getOrganizationByName(organization).hasMember(getGitHubUserByUserName(gheUserName));
    }

    @Override
    public GHOrganization getOrganizationByName(String organization) throws IOException {
        return gheClient.githubClient().getOrganization(organization);
    }
}
