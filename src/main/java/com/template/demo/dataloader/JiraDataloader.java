package com.template.demo.dataloader;

import com.atlassian.jira.rest.client.api.domain.Issue;

public interface JiraDataloader {
    Iterable<Issue> getJiraOpenedIssues(String currentUser);

    Iterable<Issue> getJiraRejectedIssues(String currentUser);

    Iterable<Issue> getJiraDoneIssues(String currentUser);

    void postCommentByKey(String jiraIssueKey, String comment);

    Issue getIssueByKey(String jiraKey);

    void updateIssueStatus(String jiraKey, String status);
}
