package com.template.demo.dataloader;

import org.kohsuke.github.GHOrganization;
import org.kohsuke.github.GHUser;

import java.io.IOException;

public interface GitHubDataloader {
    void addUserToOrganization(String gheUserName, String organization) throws IOException;

    void addUserToOrganizationRepository(String gheUserName, String repository) throws IOException;

    GHUser getGitHubUserByUserName(String gheUserName) throws IOException;

    boolean hasMember(String gheUserName, String organization) throws IOException;

    GHOrganization getOrganizationByName(String organization) throws IOException;
}
