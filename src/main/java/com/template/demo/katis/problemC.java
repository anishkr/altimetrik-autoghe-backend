package com.template.demo.katis;

import java.util.*;
import java.io.*;

public class problemC {

    public static long len;
    public static long v1;
    public static long v2;
    public static long c;
    public static long wait;
    public static long drink;
    public static int n;
    public static long[] carts;

    public static void main(String[] args) throws Exception {

        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer tok = new StringTokenizer(stdin.readLine());
        len = Long.parseLong(tok.nextToken());
        v1 = Long.parseLong(tok.nextToken());
        v2 = Long.parseLong(tok.nextToken());
        wait = Long.parseLong(tok.nextToken());
        drink = Long.parseLong(tok.nextToken());

        c = v1*v2;
        len *= c;

        n = Integer.parseInt(stdin.readLine().trim());
        tok = new StringTokenizer(stdin.readLine());
        carts = new long[n+1];
        for (int i=0; i<n; i++)
            carts[i] = v1*v2*Long.parseLong(tok.nextToken());
        carts[n] = len;

        long[] dp = new long[n+1];
        Arrays.fill(dp, -1);
        dp[n] = 0;
        int[] next = new int[n+1];
        Arrays.fill(next, -1);

        int j = n;

        for (int i=n-1; i>=0; i--) {

            long marker = carts[i] + c*(v1*wait + v2*drink);

            if (marker > len) {
                dp[i] = cost(i, n);
                continue;
            }

            while (j > i && carts[j] > marker) j--;
            if (i == j) j++;

            dp[i] = cost(i,j) + dp[j];
            next[i] = j;

            if (j+1 <= n && cost(i,j+1) + dp[j+1] < dp[i]) {
                dp[i] = cost(i,j+1) + dp[j+1];
                next[i] = j+1;
            }
        }

        // Just build our list forward.
        ArrayList<Integer> res = new ArrayList<Integer>();
        if (n > 0) {
            res.add(0);
            j = 0;
            while (next[j] != -1 && next[j] != n) {
                res.add(next[j]);
                j = next[j];
            }
        }

        // Output the result.
        System.out.println(res.size());
        StringBuffer sb = new StringBuffer();
        if (n > 0) sb.append(res.get(0));
        for (int i=1; i<res.size(); i++)
            sb.append(" "+res.get(i));
        System.out.println(sb);

    }

    public static long cost(int i, int j) {

        long d = carts[j] - carts[i];
        long full = c*(v1*wait + v2*drink);
        if (d >= full)
            return wait*c + drink*c + (d - full)/v1;
        if (d >= c*v1*wait)
            return wait*c + (d-c*v1*wait)/v2;
        return d/v1;
    }
}